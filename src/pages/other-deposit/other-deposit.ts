import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';
import { ToastController, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { TransfersProvider } from '../../providers/transfers/transfers';
import 'rxjs/add/operator/map';
import { OtherDepoSummaryPage } from '../other-depo-summary/other-depo-summary';
import { OtherDepoSummary2Page } from '../other-depo-summary2/other-depo-summary2';
import { Network } from '@ionic-native/network';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'page-other-deposit',
  templateUrl: 'other-deposit.html',
  animations: [

    //For the logo
    trigger('flyInBottomSlow', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({ transform: 'translate3d(0,2000px,0' }),
        animate('2000ms ease-in-out')
      ])
    ]),

    //For the background detail
    trigger('flyInBottomFast', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({ transform: 'translate3d(0,2000px,0)' }),
        animate('1000ms ease-in-out')
      ])
    ]),

    //For the login form
    trigger('bounceInBottom', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        animate('2000ms 200ms ease-in', keyframes([
          style({ transform: 'translate3d(0,2000px,0)', offset: 0 }),
          style({ transform: 'translate3d(0,-20px,0)', offset: 0.9 }),
          style({ transform: 'translate3d(0,0,0)', offset: 1 })
        ]))
      ])
    ]),

    //For login button
    trigger('fadeIn', [
      state('in', style({
        opacity: 1
      })),
      transition('void => *', [
        style({ opacity: 0 }),
        animate('1000ms 2000ms ease-in')
      ])
    ])
  ]
})
export class OtherDepositPage {

  public depositOtherForm: any;
  submitAttempt: boolean = false;
  jsonBody1: any;
  depositVal: any;
  messageList: any;

  api_code: any;

  from_login: any = [];

  body: any;

  data: any = [];

  jsonBody: any;
  params: any = [];

  constructor(public navCtrl: NavController, public _form: FormBuilder, private network: Network, public toastCtrl: ToastController, public navParams: NavParams, public http: Http, public transfers: TransfersProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public modalCtrl: ModalController) {
    this.from_login = this.navParams.get('value')

    this.body = Array.of(this.from_login)

    this.jsonBody = JSON.parse(this.body);

    console.log('VALUE IN Transfer CONSTRUCTOR IS' + this.from_login);
    console.log('VALUE IN ARRACONSTRUCTOR IS' + this.jsonBody);


    this.depositOtherForm = this._form.group({

      "amount": ["", Validators.compose([Validators.minLength(1), Validators.required])],
      "recipient_telco": ["", Validators.required],
      "recipient_number": ["", Validators.compose([Validators.maxLength(12), Validators.minLength(10), Validators.required])],



    })



  }


  // displayNetwork(connectionState: string) {
  //   let networkType = this.network.type

  //   this.toastCtrl.create({
  //     message: "You are now " + "" + connectionState,
  //     duration: 5000
  //   }).present();



  // }


  // ionViewDidEnter() {

  //   this.network.onConnect().subscribe(data => {
  //     console.log(data)
  //     this.displayNetwork(data.type);
  //   }, error => console.error(error));
  //   this.network.onDisconnect().subscribe(data => {
  //     console.log(data)
  //     this.displayNetwork(data.type);
  //   }, error => console.error(error));

  // }


  ionViewDidLoad() {
    console.log('ionViewDidLoad OtherDepositPage');
  }



  summary() {

    // this.depositVal = JSON.stringify(this.depositOtherForm.value);

    // this.jsonBody1 = JSON.parse(this.depositVal);


    // console.log("THIS IS THE SIGNUP VALUES" + this.jsonBody1)


    // this.jsonBody1 = {
    //   "sender_name": list.first_name + " " + list.last_name,
    //   "msisdn": list.msisdn,
    //   "telco": list.telco,
    //   "amount": this.jsonBody1.amount,
    //   "recipient_number": this.jsonBody1.recipient_number,
    //   "recipient_telco": this.jsonBody1.recipient_telco,

    //   "voucher_code": this.jsonBody1.voucher_code


    // }




     this.depositVal = JSON.stringify(this.depositOtherForm.value);
    this.jsonBody1 = JSON.parse(this.depositVal);
    console.log("THIS IS THE SIGNUP VALUES" + this.jsonBody)
    console.log("THIS IS THE billForm VALUES" + this.jsonBody1)
    console.log("THIS IS THE billForm VALUES stringified" + this.depositVal)
    console.log("LETS SEE sender_name " + this.jsonBody[0].first_name + " " + this.jsonBody[0].last_name)
    console.log("LETS SEE MOBILE NUMBER " + this.jsonBody[0].msisdn)
     console.log("LETS SEE telco " + this.jsonBody[0].telco)
   
   this.jsonBody1 = {
      "sender_name": this.jsonBody[0].first_name + " " + this.jsonBody[0].last_name,
      "msisdn": this.jsonBody[0].msisdn,
      "telco": this.jsonBody[0].telco,
     "amount": this.jsonBody1.amount,
      "recipient_number": this.jsonBody1.recipient_number,
      "recipient_telco": this.jsonBody1.recipient_telco,
      "voucher_code": this.jsonBody1.voucher_code
    }





    this.data = JSON.stringify(this.jsonBody1)
    console.log("STRINGIFIED DATA IS" + this.data);
    //  this.data = this.params

    this.body = Array.of(this.jsonBody1)

    console.log("ARRAY DATA IS" + this.body);
    console.log("PARAM DATA IS" + this.jsonBody1);

    // this.body = Array.of(this.from_login)

    let modal = this.modalCtrl.create(OtherDepoSummaryPage, {

      'value': this.body,
      'another': this.from_login
    });

    modal.present();


  }




  summary2() {

    // this.depositVal = JSON.stringify(this.depositOtherForm.value);

    // this.jsonBody1 = JSON.parse(this.depositVal);


    // console.log("THIS IS THE SIGNUP VALUES" + this.jsonBody1)


    // this.jsonBody1 = {
    //   "sender_name": list.first_name + " " + list.last_name,
    //   "msisdn": list.msisdn,
    //   "telco": list.telco,
    //   "amount": this.jsonBody1.amount,
    //   "recipient_number": this.jsonBody1.recipient_number,
    //   "recipient_telco": this.jsonBody1.recipient_telco,
    //   "voucher_code": this.jsonBody1.voucher_code

    // }




     this.depositVal = JSON.stringify(this.depositOtherForm.value);
    this.jsonBody1 = JSON.parse(this.depositVal);
    console.log("THIS IS THE SIGNUP VALUES" + this.jsonBody)
    console.log("THIS IS THE billForm VALUES" + this.jsonBody1)
    console.log("THIS IS THE billForm VALUES stringified" + this.depositVal)
    console.log("LETS SEE sender_name " + this.jsonBody.first_name + " " + this.jsonBody.last_name)
    console.log("LETS SEE MOBILE NUMBER " + this.jsonBody.msisdn)
     console.log("LETS SEE telco " + this.jsonBody.telco)
   
   this.jsonBody1 = {
      "sender_name": this.jsonBody.first_name + " " + this.jsonBody.last_name,
      "msisdn": this.jsonBody.msisdn,
      "telco": this.jsonBody.telco,
      "amount": this.jsonBody1.amount,
      "recipient_number": this.jsonBody1.recipient_number,
      "recipient_telco": this.jsonBody1.recipient_telco,
      "voucher_code": this.jsonBody1.voucher_code
    }





    this.data = JSON.stringify(this.jsonBody1)
    console.log("STRINGIFIED DATA IS" + this.data);
    //  this.data = this.params

    this.body = Array.of(this.jsonBody1)

    console.log("ARRAY DATA IS" + this.body);
    console.log("PARAM DATA IS" + this.jsonBody1);

    // this.body = Array.of(this.from_login)

    let modal = this.modalCtrl.create(OtherDepoSummary2Page, {

      'value': this.body,
      'another': this.from_login
    });

    modal.present();


  }



  //   otherDeposit() {
  //   this.transfers.otherdeposit(this.params).then((result) => {

  //     console.log(result);
  //     var jsonBody = result["_body"];
  //     console.log(jsonBody);

  //     jsonBody = JSON.parse(jsonBody);
  //      console.log(jsonBody)

  //     var desc = jsonBody["resp_desc"];
  //     var code = jsonBody["resp_code"];


  //     console.log(desc);
  //     console.log(code);

  // this.messageList = desc;
  // this.api_code = code;

  // if (this.api_code == "000"){
  //      let loader = this.loadingCtrl.create({
  //         content: "Transaction processing...",
  //         duration: 6000

  //       });

  //       loader.present();
  //       //  loader.dismissAll();

  //     }

  //      if (this.api_code != "000"){
  //      let alert = this.alertCtrl.create({
  //       title: '',
  //       subTitle: this.messageList,
  //       buttons: ['OK']
  //     });
  //     alert.present();
  //      }



  //   }, (err) => {
  //     console.log(err);
  //   });
  // }

}
