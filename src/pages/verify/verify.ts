import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ToastController, LoadingController, AlertController } from 'ionic-angular';
import { TransfersProvider } from '../../providers/transfers/transfers';
import { LoginPage } from '../login/login';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-verify',
  templateUrl: 'verify.html',
})
export class VerifyPage {

  public verifyForm: any;
  submitAttempt: boolean = false;

  messageList: any;
  from_signup: any;
  api_code: any;

  verifyVal: any;
  parse: any;
  parse1: any;
  jsonBody: any;
  jsonBody1: any;
  body: any;
  params: any = [];

  public itemList: Array<Object>;

  constructor(public navCtrl: NavController, public _form: FormBuilder, public toastCtrl: ToastController, public navParams: NavParams, public http: Http, public transfers: TransfersProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {

    // this.from_login =  this.navParams.get('value')

    // this.body = Array.of(this.from_login)

    // this.jsonBody = JSON.parse( this.body);

    //  console.log('VALUE IN BILL CONSTRUCTOR IS' + this.from_login);
    //   console.log('VALUE IN ARRACONSTRUCTOR IS' + this.jsonBody);

    this.from_signup = this.navParams.get('value')
    this.body = Array.of(this.from_signup)

    this.jsonBody = JSON.stringify(this.body);
    this.parse = JSON.parse(this.jsonBody);
    // this.parse1 = JSON.parse(this.body);
    console.log('VALUE IN CONSTRUCTOR IS' + this.from_signup);
    console.log('VALUE IN stringiy IS' + this.jsonBody);
    console.log('VALUE IN json parse IS' + this.parse);

    this.verifyForm = this._form.group({
      "auth_code": ["", Validators.required]


    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VerifyPage');
  }





  verify() {

    // this.verifyVal = JSON.stringify(this.verifyForm.value);

    // this.jsonBody1 = JSON.parse(this.verifyVal);

    // console.log("THIS IS THE Verify VALUES stringified" + this.verifyVal)
    // console.log("THIS IS THE Verify VALUES" + this.jsonBody1)
    // console.log("LETS SEE AUTHCODE " + this.jsonBody1.auth_code)
    // console.log("LETS SEE MOBILE " + list.msisdn)

    // this.params = {
    //   "mobile_number": list.msisdn,
    //   "auth_code": this.jsonBody1.auth_code


    // }



     this.verifyVal = JSON.stringify(this.verifyForm.value);
    this.jsonBody1 = JSON.parse(this.verifyVal);
    console.log("THIS IS THE SIGNUP VALUES" + this.jsonBody)
    console.log("THIS IS THE billForm VALUES" + this.jsonBody1)
    console.log("THIS IS THE billForm VALUES stringified" + this.verifyVal)
    console.log("LETS SEE sender_name " + this.jsonBody[0].first_name + " " + this.jsonBody[0].last_name)
    console.log("LETS SEE MOBILE NUMBER " + this.parse[0].msisdn)
     console.log("LETS SEE telco " + this.jsonBody[0].telco)
     console.log("LETS SEE auth_code " + this.jsonBody1.auth_code)
   
  //  this.jsonBody1 = {
  //     "sender_name": this.jsonBody.first_name + " " + this.jsonBody.last_name,
  //     "msisdn": this.jsonBody.msisdn,
  //     "telco": this.jsonBody.telco,
  //      "amount": this.jsonBody1.amount,
  //     "voucher_code": this.jsonBody1.voucher_code
  //   }

     this.params = {
      "mobile_number": this.parse[0].msisdn,
      "auth_code": this.jsonBody1.auth_code


    }


    let loader = this.loadingCtrl.create({
      content: "Please wait ..."
    
    });

    loader.present();


    this.transfers.apiVerify(this.params).then((result) => {

      console.log(result);
      var jsonBody = result["_body"];
      console.log(jsonBody);

      jsonBody = JSON.parse(jsonBody);
      console.log(jsonBody)

      var desc = jsonBody["resp_desc"];
      var code = jsonBody["resp_code"];


      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;

      loader.dismiss();

      if (this.api_code == "808") {
        let loader = this.loadingCtrl.create({
          content: "Verifying..."

        });
        loader.present();
        setTimeout(() => {
          this.navCtrl.setRoot(LoginPage,{ value: this.jsonBody });
        }, 3000);

        setTimeout(() => {
          loader.dismiss();
        }, 3000);

      }

      if (this.api_code != "801") {
        let alert = this.alertCtrl.create({
          title: "",
          subTitle: this.messageList,
          buttons: ['OK']
        });
        alert.present();
      }

    }, (err) => {

      loader.dismiss();
      this.toastCtrl.create({
        message: "Please check your internet connection",
        duration: 5000
      }).present();
      console.log(err);
    });
  }


}
