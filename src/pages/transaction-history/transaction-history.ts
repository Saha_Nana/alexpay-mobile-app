import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';



@Component({
  selector: 'page-transaction-history',
  templateUrl: 'transaction-history.html',
})
export class TransactionHistoryPage {
  list: any;


  constructor(public navParams: NavParams, public navCtrl: NavController) {

    this.list = this.navParams.get('value')
    // this.list =  this.navParams.data.value
    console.log('VALUE IN CONSTRUCTOR IS' + this.list);
  }

  ionViewDidEnter() {

    console.log('SEEn FIrst');
    console.log('DID ENTER VALUE IS' + this.list);

    this.list = this.navParams.get('value')

  }

  ionViewWillEnter() {

    this.list = this.navParams.get('value')
    console.log('WILL ENTER VALUE IS' + this.list);
    console.log('VALUE IS' + this.list);


  }

  ionViewDidLoad() {
    this.list = this.navParams.get('value')
    console.log('DID LOAD VALUE IS' + this.list);

    console.log('ionViewDidLoad TransactionHistoryPage');
  }


  ionViewWillLoad() {
    this.list = this.navParams.get('value')
    console.log('WILL LOAD VALUE IS' + this.list);
    console.log('VALUE IS' + this.list);
  }




}
