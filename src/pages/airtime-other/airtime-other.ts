import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';
import { Http } from '@angular/http';
import { ToastController, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { TransfersProvider } from '../../providers/transfers/transfers';
import { CallNumber } from '@ionic-native/call-number';
import 'rxjs/add/operator/map';
import { Network } from '@ionic-native/network';
import { OtherTopSummaryPage } from '../other-top-summary/other-top-summary';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'page-airtime-other',
  templateUrl: 'airtime-other.html',
  animations: [

    //For the logo
    trigger('flyInBottomSlow', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({ transform: 'translate3d(0,2000px,0' }),
        animate('2000ms ease-in-out')
      ])
    ]),

    //For the background detail
    trigger('flyInBottomFast', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({ transform: 'translate3d(0,2000px,0)' }),
        animate('1000ms ease-in-out')
      ])
    ]),

    //For the login form
    trigger('bounceInBottom', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        animate('2000ms 200ms ease-in', keyframes([
          style({ transform: 'translate3d(0,2000px,0)', offset: 0 }),
          style({ transform: 'translate3d(0,-20px,0)', offset: 0.9 }),
          style({ transform: 'translate3d(0,0,0)', offset: 1 })
        ]))
      ])
    ]),

    //For login button
    trigger('fadeIn', [
      state('in', style({
        opacity: 1
      })),
      transition('void => *', [
        style({ opacity: 0 }),
        animate('1000ms 2000ms ease-in')
      ])
    ])
  ]
})
export class AirtimeOtherPage {

  public airtimeOtherForm: any;
  submitAttempt: boolean = false;
  jsonBody1: any;


  messageList: any;

  api_code: any;

  from_login: any = [];

  body: any;
  airtimeVal: any;

  data: any = [];

  jsonBody: any;
  params: any = [];

   first_name: any;
  last_name: any;
  mobile: any;
  telco: any;

  constructor(public navCtrl: NavController, public _form: FormBuilder, private network: Network, public toastCtrl: ToastController, public navParams: NavParams, public http: Http, public transfers: TransfersProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public modalCtrl: ModalController, private callNumber: CallNumber) {


    this.from_login = this.navParams.get('value')

    this.body = Array.of(this.from_login)

    this.jsonBody = JSON.parse(this.body);

    console.log('VALUE IN AIRTIME OTHER CONSTRUCTOR IS' + this.from_login);
    console.log('VALUE IN ARRACONSTRUCTOR IS' + this.jsonBody);

     this.first_name = this.jsonBody[0].first_name
    this.last_name = this.jsonBody[0].last_name
    this.mobile = this.jsonBody[0].msisdn
    this.telco = this.jsonBody[0].telco
    
    console.log("Name " +  this.first_name + this.last_name + this.mobile +  this.telco  )


    this.params.top_up_option = "Others"


    this.airtimeOtherForm = this._form.group({
      "voucher_code": ["", Validators.compose([Validators.maxLength(6), Validators.minLength(6)])],
      "amount": ["", Validators.compose([Validators.minLength(1), Validators.required])],
      "recipient_telco": ["", Validators.required],
      "recipient_number": ["", Validators.compose([Validators.maxLength(12), Validators.minLength(10), Validators.required])],
      "top_up_option": ["Others"]


    })

  }

  // displayNetwork(connectionState: string) {
  //   let networkType = this.network.type

  //   this.toastCtrl.create({
  //     message: "You are now " + "" + connectionState,
  //     duration: 5000
  //   }).present();



  // }


  // ionViewDidEnter() {

  //   this.network.onConnect().subscribe(data => {
  //     console.log(data)
  //     this.displayNetwork(data.type);
  //   }, error => console.error(error));
  //   this.network.onDisconnect().subscribe(data => {
  //     console.log(data)
  //     this.displayNetwork(data.type);
  //   }, error => console.error(error));

  // }



  ionViewDidLoad() {
    console.log('ionViewDidLoad AirtimeOtherPage');
  }

  call(n: string) {
    this.callNumber.callNumber(n, true)
      .then(() => console.log('Launched dialer!'))
      .catch(() => console.log('Error launching dialer'));
  }


  summary() {

    // this.airtimeVal = JSON.stringify(this.airtimeOtherForm.value);
    // this.jsonBody1 = JSON.parse(this.airtimeVal);
    // console.log("THIS IS THE SIGNUP VALUES" + this.jsonBody1)
    // this.jsonBody1 = {
    //   "sender_name": list.first_name + " " + list.last_name,
    //   "msisdn": list.msisdn,
    //   "telco": list.telco,
    //   "amount": this.jsonBody1.amount,
    //   "recipient_number": this.jsonBody1.recipient_number,
    //   "recipient_telco": this.jsonBody1.recipient_telco,
    //   "top_up_option": this.jsonBody1.top_up_option,
    //   "voucher_code": this.jsonBody1.voucher_code

    // }


    this.airtimeVal = JSON.stringify(this.airtimeOtherForm.value);
    this.jsonBody1 = JSON.parse(this.airtimeVal);
    console.log("THIS IS THE SIGNUP VALUES" + this.jsonBody)
    console.log("THIS IS THE airtimeOtherForm VALUES" + this.jsonBody1)
    console.log("THIS IS THE airtimeOtherForm VALUES stringified" + this.airtimeVal)
    console.log("LETS SEE sender_name " + this.jsonBody[0].first_name + " " + this.jsonBody[0].last_name)
    console.log("LETS SEE MOBILE NUMBER " + this.jsonBody[0].msisdn)
     console.log("LETS SEE telco " + this.jsonBody[0].telco)
   
   this.jsonBody1 = {
      "sender_name": this.jsonBody[0].first_name + " " + this.jsonBody[0].last_name,
      "msisdn": this.jsonBody[0].msisdn,
      "telco": this.jsonBody[0].telco,
      "amount": this.jsonBody1.amount,
      "recipient_number": this.jsonBody1.recipient_number,
      "recipient_telco": this.jsonBody1.recipient_telco,
      "top_up_option": this.jsonBody1.top_up_option,
      "voucher_code": this.jsonBody1.voucher_code
    }





    this.data = JSON.stringify(this.jsonBody1)
    console.log("STRINGIFIED DATA IS" + this.data);
    //  this.data = this.params

    this.body = Array.of(this.jsonBody1)

    console.log("ARRAY DATA IS" + this.body);
    console.log("PARAM DATA IS" + this.jsonBody1);


    //    this.navCtrl.push(SummaryPage, {

    //   'value': this.data
    // });

    let modal = this.modalCtrl.create(OtherTopSummaryPage, {

      'value': this.body,
      'another': this.from_login
    });

    modal.present();


  }


  //   sendtop() {
  //   this.transfers.airtimeOther(this.params).then((result) => {

  //     console.log(result);
  //     var jsonBody = result["_body"];
  //     console.log(jsonBody);

  //     jsonBody = JSON.parse(jsonBody);
  //      console.log(jsonBody)

  //     var desc = jsonBody["resp_desc"];
  //     var code = jsonBody["resp_code"];


  //     console.log(desc);
  //     console.log(code);

  // this.messageList = desc;
  // this.api_code = code;

  //  if (this.api_code == "000"){
  //      let loader = this.loadingCtrl.create({
  //         content: "Transaction processing...",
  //         duration: 6000

  //       });

  //       loader.present();
  //       //  loader.dismissAll();

  //     }

  //      if (this.api_code != "000"){
  //      let alert = this.alertCtrl.create({
  //       title: 'Error',
  //       subTitle: this.messageList,
  //       buttons: ['OK']
  //     });
  //     alert.present();
  //      }



  //   }, (err) => {
  //     console.log(err);
  //   });
  // }


  // presentToast() {
  //   let toast = this.toastCtrl.create({
  //     message: this.messageList,
  //     duration: 6000
  //   });

  //   if (this.api_code == "000"){
  //    let loader = this.loadingCtrl.create({
  //       content: "Transaction processing...",
  //       duration: 6000

  //     });

  //     loader.present();
  //     //  loader.dismissAll();

  //   }

  //    if (this.api_code != "000"){
  //    let alert = this.alertCtrl.create({
  //     title: 'Error',
  //     subTitle: this.messageList,
  //     buttons: ['OK']
  //   });
  //   alert.present();
  //    }
  //   toast.present();


  // }

}
