import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';
import { Http } from '@angular/http';
import { LoginPage } from '../login/login';
import { ToastController, LoadingController, AlertController } from 'ionic-angular';
import { TransfersProvider } from '../../providers/transfers/transfers';
import 'rxjs/add/operator/map';
import { Network } from '@ionic-native/network';



@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
  animations: [

    //For the logo
    trigger('flyInBottomSlow', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({ transform: 'translate3d(0,2000px,0' }),
        animate('2000ms ease-in-out')
      ])
    ]),

    //For the background detail
    trigger('flyInBottomFast', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({ transform: 'translate3d(0,2000px,0)' }),
        animate('1000ms ease-in-out')
      ])
    ]),

    //For the login form
    trigger('bounceInBottom', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        animate('2000ms 200ms ease-in', keyframes([
          style({ transform: 'translate3d(0,2000px,0)', offset: 0 }),
          style({ transform: 'translate3d(0,-20px,0)', offset: 0.9 }),
          style({ transform: 'translate3d(0,0,0)', offset: 1 })
        ]))
      ])
    ]),

    //For login button
    trigger('fadeIn', [
      state('in', style({
        opacity: 1
      })),
      transition('void => *', [
        style({ opacity: 0 }),
        animate('1000ms 2000ms ease-in')
      ])
    ])
  ]
})
export class ProfilePage {

  messageList: any;

  api_code: any;
  list: any = [];
  mobile_number: any;
  data: any;
  displayData: any;

  check: any;

  from_login: any = [];

  body: any;
  first_name: any;
  last_name: any;
  mobile: any;

  
  jsonBody: any;
  params: any = [];

  constructor(public navCtrl: NavController, private network: Network, public toastCtrl: ToastController, public navParams: NavParams, public http: Http, public transfers: TransfersProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {
    this.from_login = this.navParams.get('value')

    this.body = Array.of(this.from_login)

    this.jsonBody = JSON.parse(this.body);

    console.log('VALUE IN BILL CONSTRUCTOR IS' + this.from_login);
    console.log('VALUE IN ARRACONSTRUCTOR IS' + this.jsonBody);

    this.first_name = this.jsonBody[0].first_name
    this.last_name = this.jsonBody[0].last_name
    this.mobile = this.jsonBody[0].msisdn
    
    console.log("Name " +  this.first_name + this.last_name + this.mobile  )

  }


  // displayNetwork(connectionState: string) {
  //   this.network.type
  //   // let networkType = this.network.type

  //   this.toastCtrl.create({
  //     message: "You are now " + "" + connectionState,
  //     duration: 5000
  //   }).present();



  // }


  ionViewDidEnter() {

    // this.network.onConnect().subscribe(data => {
    //   console.log(data)
    //   this.displayNetwork(data.type);
    // }, error => console.error(error));
    // this.network.onDisconnect().subscribe(data => {
    //   console.log(data)
    //   this.displayNetwork(data.type);
    // }, error => console.error(error));

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }




  profile() {


 
    console.log("LETS SEE MOBILE NUMBER " + this.jsonBody[0].msisdn)
     console.log("LETS SEE telco " + this.jsonBody[0].telco)


    let loader = this.loadingCtrl.create({
      content: "Please wait ..."

    });

    loader.present();

    this.params = {

      "msisdn": this.jsonBody[0].msisdn,
      "pin": this.params.pin,
      "new_pin": this.params.new_pin
    }




    this.transfers.profile(this.params).then((result) => {

      console.log(result);
      var jsonBody = result["_body"];
      console.log(jsonBody);

      jsonBody = JSON.parse(jsonBody);
      console.log(jsonBody)

      var desc = jsonBody["resp_desc"];
      var code = jsonBody["resp_code"];


      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;

      loader.dismiss();


if (this.api_code == "200") {
    
    let alert = this.alertCtrl.create({
          title: '',
          subTitle: this.messageList,
          buttons: ['OK']

        });


        let loader = this.loadingCtrl.create({
          content: "Redirecting to login...",

        });
        loader.present();
        setTimeout(() => {
          this.navCtrl.setRoot(LoginPage,{ value: this.jsonBody });
        }, 3000);

        setTimeout(() => {
          loader.dismiss();
        }, 3000);

        alert.present();

      }



      if (this.api_code == "201") {
        let alert = this.alertCtrl.create({
          title: '',
          subTitle: this.messageList,
          buttons: ['OK']
        });
        alert.present();
      }



    }, (err) => {
      loader.dismiss();
      this.toastCtrl.create({
        message: "Please check your internet connection",
        duration: 5000
      }).present();

      console.log(err);
    });
  }


}
