import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';
// import { TransferFundsPage } from '../transfer-funds/transfer-funds';
import { ToastController, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { TransfersProvider } from '../../providers/transfers/transfers';
import { SummaryPage } from '../summary/summary';
import { Summary2Page } from '../summary2/summary2';
import { CallNumber } from '@ionic-native/call-number';
import { Network } from '@ionic-native/network';
import 'rxjs/add/operator/map';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// declare var window;

@Component({
  selector: 'page-transfer-funds',
  templateUrl: 'transfer-funds.html',
  animations: [

    //For the logo
    trigger('flyInBottomSlow', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({ transform: 'translate3d(0,2000px,0' }),
        animate('2000ms ease-in-out')
      ])
    ]),

    //For the background detail
    trigger('flyInBottomFast', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({ transform: 'translate3d(0,2000px,0)' }),
        animate('1000ms ease-in-out')
      ])
    ]),

    //For the login form
    trigger('bounceInBottom', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        animate('2000ms 200ms ease-in', keyframes([
          style({ transform: 'translate3d(0,2000px,0)', offset: 0 }),
          style({ transform: 'translate3d(0,-20px,0)', offset: 0.9 }),
          style({ transform: 'translate3d(0,0,0)', offset: 1 })
        ]))
      ])
    ]),

    //For login button
    trigger('fadeIn', [
      state('in', style({
        opacity: 1
      })),
      transition('void => *', [
        style({ opacity: 0 }),
        animate('1000ms 2000ms ease-in')
      ])
    ])
  ]
})

export class TransferFundsPage {


  public transferForm: any;
  submitAttempt: boolean = false;
  jsonBody1: any;
  depositVal: any;

  messageList: any;

  api_code: any;

  from_login: any = [];

  body: any;

  data: any = [];

  jsonBody: any;
  params: any = [];


  another: any;
   first_name: any;
  last_name: any;
  mobile: any;
  telco: any;


  constructor(public navCtrl: NavController, public _form: FormBuilder, public toastCtrl: ToastController, private network: Network, public navParams: NavParams, public http: Http, public transfers: TransfersProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public modalCtrl: ModalController, private callNumber: CallNumber) {
    this.from_login = this.navParams.get('value')

    this.body = Array.of(this.from_login)

    this.jsonBody = JSON.parse(this.body);

    console.log('VALUE IN Transfer CONSTRUCTOR IS' + this.from_login);
    console.log('VALUE IN ARRACONSTRUCTOR IS' + this.jsonBody);

     this.first_name = this.jsonBody[0].first_name
    this.last_name = this.jsonBody[0].last_name
    this.mobile = this.jsonBody[0].msisdn
    this.telco = this.jsonBody[0].telco
    
    console.log("Name " +  this.first_name + this.last_name + this.mobile +  this.telco  )



    this.transferForm = this._form.group({

      "voucher_code": ["", Validators.compose([Validators.maxLength(6), Validators.minLength(6)])],
      "amount": ["", Validators.compose([Validators.minLength(1), Validators.required])],
      "recipient_telco": ["", Validators.required],
      "recipient_number": ["", Validators.compose([Validators.maxLength(12), Validators.minLength(10), Validators.required])],



    })

  }


  // displayNetwork(connectionState: string) {
  //   let networkType = this.network.type

  //   this.toastCtrl.create({
  //     message: "You are now " + "" + connectionState,
  //     duration: 5000
  //   }).present();



  // }


  // ionViewDidEnter() {

  //   this.network.onConnect().subscribe(data => {
  //     console.log(data)
  //     this.displayNetwork(data.type);
  //   }, error => console.error(error));
  //   this.network.onDisconnect().subscribe(data => {
  //     console.log(data)
  //     this.displayNetwork(data.type);
  //   }, error => console.error(error));

  // }

  ionViewDidLoad() {

  }






  summary() {

   




     this.depositVal = JSON.stringify(this.transferForm.value);
    this.jsonBody1 = JSON.parse(this.depositVal);
    console.log("THIS IS THE SIGNUP VALUES" + this.jsonBody)
    console.log("THIS IS THE billForm VALUES" + this.jsonBody1)
    console.log("THIS IS THE billForm VALUES stringified" + this.depositVal)
    console.log("LETS SEE sender_name " + this.jsonBody[0].first_name + " " + this.jsonBody[0].last_name)
    console.log("LETS SEE MOBILE NUMBER " + this.jsonBody[0].msisdn)
     console.log("LETS SEE telco " + this.jsonBody[0].telco)
   
   this.jsonBody1 = {
      "sender_name": this.jsonBody[0].first_name + " " + this.jsonBody[0].last_name,
      "msisdn": this.jsonBody[0].msisdn,
      "telco": this.jsonBody[0].telco,
       "amount": this.jsonBody1.amount,
      "recipient_number": this.jsonBody1.recipient_number,
      "recipient_telco": this.jsonBody1.recipient_telco,
      "voucher_code": this.jsonBody1.voucher_code
    }




    this.data = JSON.stringify(this.jsonBody1)
    console.log("STRINGIFIED DATA IS" + this.data);
    //  this.data = this.params

    this.body = Array.of(this.jsonBody1)

    console.log("ARRAY DATA IS" + this.body);
    console.log("PARAM DATA IS" + this.jsonBody1);

    // this.body = Array.of(this.from_login)

    let modal = this.modalCtrl.create(SummaryPage, {

      'value': this.body,
      'another': this.from_login
    });

    modal.present();


  }



  call(n: string) {
    this.callNumber.callNumber(n, true)
      .then(() => console.log('Launched dialer!'))
      .catch(() => console.log('Error launching dialer'));
  }




  summary2() {


    // this.depositVal = JSON.stringify(this.transferForm.value);

    // this.jsonBody1 = JSON.parse(this.depositVal);


    // console.log("THIS IS THE SIGNUP VALUES" + this.jsonBody1)

    // this.jsonBody1 = {
    //   "sender_name": list.first_name + " " + list.last_name,
    //   "msisdn": list.msisdn,
    //   "telco": list.telco,
    //   "amount": this.jsonBody1.amount,
    //   "recipient_number": this.jsonBody1.recipient_number,
    //   "recipient_telco": this.jsonBody1.recipient_telco,
    //   "voucher_code": this.jsonBody1.voucher_code



    // }



        this.depositVal = JSON.stringify(this.transferForm.value);
    this.jsonBody1 = JSON.parse(this.depositVal);
    console.log("THIS IS THE SIGNUP VALUES" + this.jsonBody)
    console.log("THIS IS THE billForm VALUES" + this.jsonBody1)
    console.log("THIS IS THE billForm VALUES stringified" + this.depositVal)
    console.log("LETS SEE sender_name " + this.jsonBody[0].first_name + " " + this.jsonBody[0].last_name)
    console.log("LETS SEE MOBILE NUMBER " + this.jsonBody[0].msisdn)
     console.log("LETS SEE telco " + this.jsonBody[0].telco)
   
   this.jsonBody1 = {
      "sender_name": this.jsonBody[0].first_name + " " + this.jsonBody[0].last_name,
      "msisdn": this.jsonBody[0].msisdn,
      "telco": this.jsonBody[0].telco,
     "amount": this.jsonBody1.amount,
      "recipient_number": this.jsonBody1.recipient_number,
      "recipient_telco": this.jsonBody1.recipient_telco,
      "voucher_code": this.jsonBody1.voucher_code
    }




    this.data = JSON.stringify(this.jsonBody1)
    console.log("STRINGIFIED DATA IS" + this.data);
    //  this.data = this.params

    this.body = Array.of(this.jsonBody1)

    console.log("ARRAY DATA IS" + this.body);
    console.log("PARAM DATA IS" + this.jsonBody1);

    // this.body = Array.of(this.from_login)

    let modal = this.modalCtrl.create(Summary2Page, {

      'value': this.body,
      'another': this.from_login

    });

    modal.present();


  }



}
