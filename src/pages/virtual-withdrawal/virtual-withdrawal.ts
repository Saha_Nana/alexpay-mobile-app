import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {Http} from '@angular/http';
import {trigger, state,style,animate,transition,keyframes} from '@angular/animations';
import { ToastController,LoadingController,AlertController,ModalController   } from 'ionic-angular';
import { TransfersProvider } from '../../providers/transfers/transfers';
import {Network} from '@ionic-native/network';
import 'rxjs/add/operator/map';
import { SelfWithSummaryPage } from '../self-with-summary/self-with-summary';


 
@Component({
  selector: 'page-virtual-withdrawal',
  templateUrl: 'virtual-withdrawal.html',

animations: [
 
    //For the logo
    trigger('flyInBottomSlow', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({transform: 'translate3d(0,2000px,0'}),
        animate('2000ms ease-in-out')
      ])
    ]),
 
    //For the background detail
    trigger('flyInBottomFast', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({transform: 'translate3d(0,2000px,0)'}),
        animate('1000ms ease-in-out')
      ])
    ]),
 
    //For the login form
    trigger('bounceInBottom', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        animate('2000ms 200ms ease-in', keyframes([
          style({transform: 'translate3d(0,2000px,0)', offset: 0}),
          style({transform: 'translate3d(0,-20px,0)', offset: 0.9}),
          style({transform: 'translate3d(0,0,0)', offset: 1}) 
        ]))
      ])
    ]),
 
    //For login button
    trigger('fadeIn', [
      state('in', style({
        opacity: 1
      })),
      transition('void => *', [
        style({opacity: 0}),
        animate('1000ms 2000ms ease-in')
      ])
    ])
  ]
})



export class VirtualWithdrawalPage {

   messageList: any;

 api_code: any;

 from_login: any = [];

  body: any;

  data: any = [ ];

  jsonBody: any;
  params: any = [];


  constructor(public navCtrl: NavController,public toastCtrl: ToastController, public navParams: NavParams, public http: Http, public transfers: TransfersProvider,public loadingCtrl:LoadingController,public alertCtrl: AlertController,public modalCtrl: ModalController,private network: Network) {


        this.from_login =  this.navParams.get('value')

        this.body = Array.of(this.from_login)

        this.jsonBody = JSON.parse( this.body);

         console.log('VALUE IN Transfer CONSTRUCTOR IS' + this.from_login);
          console.log('VALUE IN ARRACONSTRUCTOR IS' + this.jsonBody);


 }


//   displayNetwork(connectionState: string){
//   let networkType = this.network.type

//    this.toastCtrl.create({
//         message: "You are now " +"" + connectionState  ,
//          duration: 5000
//  }).present();



// }


  ionViewDidEnter() {

    // this.network.onConnect().subscribe(data=> {console.log(data)
    //    this.displayNetwork(data.type);},error => console.error(error));
    // this.network.onDisconnect().subscribe(data=> {console.log(data)
    //    this.displayNetwork(data.type);},error => console.error(error));
  
}


  ionViewDidLoad() {
    console.log('ionViewDidLoad VirtualWithdrawalPage');
  }





summary(){


   this.params = {
    "sender_name": this.jsonBody[0].first_name + " " + this.jsonBody[0].last_name,
    "msisdn":  this.jsonBody[0].msisdn,
    "telco": this.params.telco, 
    "amount": this.params.amount,  

      
 }



 this.data = JSON.stringify(this.params)
 console.log("STRINGIFIED DATA IS" + this.data);
  //  this.data = this.params
   
   this.body = Array.of(this.params)
  
    console.log("ARRAY DATA IS" + this.body);
    console.log("PARAM DATA IS" + this.params);
     


  let modal = this.modalCtrl.create(SelfWithSummaryPage, {

    'value': this.body,
    'another': this.from_login
  });

modal.present();


}



}
