import { Component } from '@angular/core';

import {trigger, state,style,animate,transition,keyframes} from '@angular/animations';

import { NavController, NavParams,AlertController,LoadingController,ToastController } from 'ionic-angular';

import {Network} from '@ionic-native/network';
// import { TabsPage } from '../tabs/tabs';
import { SignupPage } from '../signup/signup';
import { LoginPage } from '../login/login';




@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',

animations: [
 
    //For the logo
    trigger('flyInBottomSlow', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({transform: 'translate3d(0,2000px,0'}),
        animate('2000ms ease-in-out')
      ])
    ]),
 
    //For the background detail
    trigger('flyInBottomFast', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({transform: 'translate3d(0,2000px,0)'}),
        animate('1000ms ease-in-out')
      ])
    ]),
 
    //For the login form
    trigger('bounceInBottom', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        animate('2000ms 200ms ease-in', keyframes([
          style({transform: 'translate3d(0,2000px,0)', offset: 0}),
          style({transform: 'translate3d(0,-20px,0)', offset: 0.9}),
          style({transform: 'translate3d(0,0,0)', offset: 1}) 
        ]))
      ])
    ]),
 
    //For login button
    trigger('fadeIn', [
      state('in', style({
        opacity: 1
      })),
      transition('void => *', [
        style({opacity: 0}),
        animate('1000ms 2000ms ease-in')
      ])
    ])
  ]
})


export class WelcomePage {
  //  splash = true;
    from_login: any = [];
  body: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public toastCtrl: ToastController,public alertCtrl: AlertController,public loadingCtrl: LoadingController,private network: Network) {
 
 this.from_login =  this.navParams.data

      this.body = Array.of(this.from_login)

 }

//   displayNetwork(connectionState: string){
//   let networkType = this.network.type

//    this.toastCtrl.create({
//         message: "You are now " +"" + connectionState  ,
//          duration: 5000
//  }).present();



// }
 ionViewDidLoad() {
    // setTimeout(() => this.splash = false, 4000);
    console.log('ionViewDidLoad WelcomePage');
  }


  ionViewDidEnter() {

    // this.network.onConnect().subscribe(data=> {console.log(data)
    //    this.displayNetwork(data.type);},error => console.error(error));
    // this.network.onDisconnect().subscribe(data=> {console.log(data)
    //    this.displayNetwork(data.type);},error => console.error(error));
  
}

  




  signup(){

     let loader = this.loadingCtrl.create({
        content: "loading...", 
        duration: 1000
     
       
      
      });

      loader.present();

    this.navCtrl.push(SignupPage)
  }

   login(){

      let loader = this.loadingCtrl.create({
        content: "loading...",
        duration: 1000
     
       
      
      });

      loader.present();

    this.navCtrl.push(LoginPage)
  }

}
