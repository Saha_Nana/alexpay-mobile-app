import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';
import { Http } from '@angular/http';
import { TabsPage } from '../tabs/tabs';
import { ContactPage } from '../contact/contact';
import { ToastController, LoadingController, AlertController } from 'ionic-angular';
import { TransfersProvider } from '../../providers/transfers/transfers';
import { Device } from '@ionic-native/device';
import { Network } from '@ionic-native/network';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import 'rxjs/add/operator/map';


interface deviceInterface {
  id?: string,
  model?: string,
  cordova?: string,
  platform?: string,
  version?: string,
  manufacturer?: string,
  serial?: string,
  // imei?: string,
  isVirtual?: boolean,
 
};



@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  animations: [

    //For the logo
    trigger('flyInBottomSlow', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({ transform: 'translate3d(0,2000px,0' }),
        animate('2000ms ease-in-out')
      ])
    ]),

    //For the background detail
    trigger('flyInBottomFast', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({ transform: 'translate3d(0,2000px,0)' }),
        animate('1000ms ease-in-out')
      ])
    ]),

    //For the login form
    trigger('bounceInBottom', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        animate('1000ms 200ms ease-in', keyframes([
          style({ transform: 'translate3d(0,2000px,0)', offset: 0 }),
          style({ transform: 'translate3d(0,-20px,0)', offset: 0.9 }),
          style({ transform: 'translate3d(0,0,0)', offset: 1 })
        ]))
      ])
    ]),

    //For login button
    trigger('fadeIn', [
      state('in', style({
        opacity: 1
      })),
      transition('void => *', [
        style({ opacity: 0 }),
        animate('1000ms 2000ms ease-in')
      ])
    ])
  ]
})
export class LoginPage {

  public loginForm: any;
  submitAttempt: boolean = false;
  loginVal: any;
  jsonBody: any;
  body: any;
  parse:any;
   params:any;
  imei: any;
  messageList: any;

  api_code: any;
 from_signup: any;
  msisdn: string;
  pin: string;
  retrieve: string;
  public deviceInfo: deviceInterface = {};
  constructor(public navCtrl: NavController, public toastCtrl: ToastController, public _form: FormBuilder, public navParams: NavParams, public http: Http, public transfers: TransfersProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController, private network: Network,private device: Device) {
    
    this.deviceInfo.id = this.device.uuid;
    // this.deviceInfo.model = this.device.model;
    // this.deviceInfo.cordova = this.device.cordova;
    // this.deviceInfo.platform = this.device.platform;
    // this.deviceInfo.version = this.device.version;
    // this.deviceInfo.manufacturer = this.device.manufacturer;
    // this.deviceInfo.serial = this.device.serial;
    // this.deviceInfo.isVirtual = this.device.isVirtual;

    console.log("UUID IN LOGIN " + this.deviceInfo.id )
    // console.log("MODEL " + this.deviceInfo.model )
    // console.log("cordova " + this.deviceInfo.cordova )
    // console.log("platform " + this.deviceInfo.platform )
    // console.log("version " + this.deviceInfo.version )
    // console.log("manufacturer " + this.deviceInfo.manufacturer )
    // console.log("serial " + this.deviceInfo.serial )
    // console.log("isVirtual " + this.deviceInfo.version )

    //  this.from_signup = this.navParams.get('value')

    //  this.body = Array.of(this.from_signup)

    // this.jsonBody = JSON.stringify(this.body);
    // this.parse = JSON.parse(this.jsonBody);
    // // this.parse1 = JSON.parse(this.body);
    // console.log('VALUE IN CONSTRUCTOR IS' + this.from_signup);
    // console.log('VALUE IN stringiy IS' + this.jsonBody);
    // console.log('VALUE IN json parse IS' + this.parse);

    //  this.from_signup = this.navParams.get('value')
 
   
    //  this.body = Array.of(this.from_signup)

    // this.parse = JSON.parse(this.body);

    // this.imei = this.jsonBody[0].imei
    

    // console.log('VALUE IN LOGIN STRINGIFY IS' + this.from_signup);
    // console.log('VALUE IN stringiy IS' + this.jsonBody);
    // console.log('IMEI VALUE IN LOGIN' + this.imei);
    
    this.loginForm = this._form.group({

      "pin": ["", Validators.compose([Validators.maxLength(4), Validators.required])],
      "msisdn": ["", Validators.compose([Validators.maxLength(12), Validators.minLength(10), Validators.required])]


    })

  }




  ionViewDidEnter() {


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }



  pin1() {


    this.navCtrl.push(ContactPage)
  }


  login() {

    this.loginVal = JSON.stringify(this.loginForm.value);

    this.jsonBody = JSON.parse(this.loginVal);
    console.log("THIS IS THE SIGNUP VALUES" + this.jsonBody)
    console.log("THIS IS THE LOGIN VALUES stringified" + this.loginVal)
    console.log("THIS IS THE Verify VALUES" + this.jsonBody)
    console.log("LETS SEE MOBILE NUMBER " + this.jsonBody.msisdn)
      console.log("LETS SEE PIN " + this.jsonBody.pin)
    console.log("LETS SEE IMEI" + this.imei)

    this.params = {
     
   "msisdn": this.jsonBody.msisdn,
    "pin": this.jsonBody.pin,
    "imei": this.deviceInfo.id


    }


    let loader = this.loadingCtrl.create({
      content: "Please wait ..."
      
    });

    loader.present();

    this.transfers.retrieve(this.jsonBody).then((result) => {
      var body = result["_body"];
      body = JSON.parse(body);

      this.retrieve = body
      this.retrieve = JSON.stringify(body)


      console.log('LETS SEE THE DATA RETRIEVED ' + this.retrieve);
    });



    this.transfers.login(this.params).then((result) => {

      console.log(result);
      var jsonBody = result["_body"];
      console.log(jsonBody);

      jsonBody = JSON.parse(jsonBody);
      console.log(jsonBody)

      var desc = jsonBody["resp_desc"];
      var code = jsonBody["resp_code"];


      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;


      loader.dismiss();
      if (this.api_code == "200") {
        let loader = this.loadingCtrl.create({
          content: "Login processing...",
          duration: 5000

        });

        loader.present();

        setTimeout(() => {
          this.navCtrl.setRoot(TabsPage,

            { value: this.retrieve });

        }, 3000);

        setTimeout(() => {
          loader.dismiss();
        }, 3000);

      }

      if (this.api_code == "201") {
        let alert = this.alertCtrl.create({
          title: "",
          subTitle: this.messageList,
          buttons: ['OK']
        });
        alert.present();
      }

      if (this.api_code == "118") {
        let alert = this.alertCtrl.create({
          title: "",
          subTitle: this.messageList,
          buttons: ['OK']
        });
        alert.present();
      }

      if (this.api_code == "103") {
        let alert = this.alertCtrl.create({
          title: "",
          subTitle: this.messageList,
          buttons: ['OK']
        });
        alert.present();
      }


      if (this.api_code == "222") {
        let alert = this.alertCtrl.create({
          title: "",
          subTitle: this.messageList,
          buttons: ['OK']
        });
        alert.present();
      }

      if (this.api_code == "001") {
        let alert = this.alertCtrl.create({
          title: "",
          subTitle: this.messageList,
          buttons: ['OK']
        });
        alert.present();
      }
    },

      (err) => {
          loader.dismiss();
        this.toastCtrl.create({
          message: "Please check your internet connection",
          duration: 5000
        }).present();

        console.log(err);
      });
  }



}
