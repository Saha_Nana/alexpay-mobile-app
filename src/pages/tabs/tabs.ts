import { Component } from '@angular/core';
import { NavController, NavParams ,ToastController} from 'ionic-angular';
import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { HistoryPage } from '../history/history';
import {Network} from '@ionic-native/network';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  from_login: any = [];

  tab1Root = HomePage;
  tab2Root = HistoryPage;
  tab3Root = ContactPage;


  

  constructor(public navCtrl: NavController, public toastCtrl: ToastController,public navParams: NavParams,private network: Network) {

     this.from_login =  this.navParams.get('value')

         console.log('VALUE IN TABS CONSTRUCTOR IS' + this.from_login);

         this.tab1Root = HomePage;
         this.tab2Root = HistoryPage;
         this.tab3Root = ContactPage;

  }
//     displayNetwork(connectionState: string){
//   let networkType = this.network.type

//    this.toastCtrl.create({
//         message: "You are now " +"" + connectionState  ,
//          duration: 5000
//  }).present();



// }


//   ionViewDidEnter() {

//     this.network.onConnect().subscribe(data=> {console.log(data)
//        this.displayNetwork(data.type);},error => console.error(error));
//     this.network.onDisconnect().subscribe(data=> {console.log(data)
//        this.displayNetwork(data.type);},error => console.error(error));
  
// }
}
