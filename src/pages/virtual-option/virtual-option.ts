import { Component } from '@angular/core';
import {  NavController, App,NavParams } from 'ionic-angular';
import { VirtualDepositPage } from '../virtual-deposit/virtual-deposit';
import { VirtualWithdrawalPage } from '../virtual-withdrawal/virtual-withdrawal';
import { CustomerBalancePage } from '../customer-balance/customer-balance';
import { LoadingController,AlertController,ToastController   } from 'ionic-angular';
import { WelcomePage } from '../welcome/welcome';
import { DepositPage } from '../deposit/deposit';
import { OtherDepositPage } from '../other-deposit/other-deposit';
import { TransferFundsPage } from '../transfer-funds/transfer-funds';
import {Network} from '@ionic-native/network';



@Component({
  selector: 'page-virtual-option',
  templateUrl: 'virtual-option.html',
})
export class VirtualOptionPage {

   messageList: any;

 api_code: any;

 from_login: any = [];

  body: any;

  data: any = [ ];

  jsonBody: any;
  params: any = [];


 constructor(public navCtrl: NavController,public toastCtrl: ToastController,public navParams: NavParams,public loadingCtrl:LoadingController,public alertCtrl: AlertController, public _app: App,private network: Network) {
 
      this.from_login =  this.navParams.get('value')

        this.body = Array.of(this.from_login)

        this.jsonBody = JSON.parse( this.body);

         console.log('VALUE IN VIRTUAL OPTIONS CONSTRUCTOR IS' + this.from_login);
          console.log('VALUE IN ARRACONSTRUCTOR IS' + this.jsonBody);



 }



 displayNetwork(connectionState: string){
  let networkType = this.network.type

   this.toastCtrl.create({
        message: "You are now " +"" + connectionState  ,
         duration: 5000
 }).present();



}


  ionViewDidEnter() {

    this.network.onConnect().subscribe(data=> {console.log(data)
       this.displayNetwork(data.type);},error => console.error(error));
    this.network.onDisconnect().subscribe(data=> {console.log(data)
       this.displayNetwork(data.type);},error => console.error(error));
  
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad VirtualOptionPage');
  }


  deposit(){
    this.navCtrl.push(DepositPage,{'value': this.body} )
  }

   transfer_out(){
    this.navCtrl.push(OtherDepositPage,{'value': this.body})
  }

   transfer_2(){
    this.navCtrl.push(TransferFundsPage,{value: this.from_login})  
  }

  withdraw(){
    this.navCtrl.push(VirtualWithdrawalPage,{'value': this.body} )
  }

  balance(){

    this.navCtrl.push(CustomerBalancePage,{'value': this.body} )


  }


   showConfirm() {
    let confirm = this.alertCtrl.create({
      title: 'Logout?',
      message: 'Do you agree to logout ?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
             let loader = this.loadingCtrl.create({
        content: "loggin out..."
        
       
      
      });

            loader.present();
     
       setTimeout(() => {
     this._app.getRootNav().setRoot(WelcomePage);
  }, 3000);

    setTimeout(() => {
    loader.dismiss();
  }, 3000);

          }
        }
      ]
    });
    confirm.present();
  }


}
