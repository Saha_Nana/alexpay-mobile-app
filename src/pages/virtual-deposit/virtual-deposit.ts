import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { DepositPage } from '../deposit/deposit';
import { OtherDepositPage } from '../other-deposit/other-deposit';
import { Network } from '@ionic-native/network';



@Component({
  selector: 'page-virtual-deposit',
  templateUrl: 'virtual-deposit.html',
})
export class VirtualDepositPage {
  
  messageList: any;

  api_code: any;

  from_login: any = [];

  body: any;

  data: any = [];

  jsonBody: any;
  params: any = [];


  constructor(public navCtrl: NavController, public navParams: NavParams, private network: Network, public toastCtrl: ToastController) {
    this.from_login = this.navParams.get('value')

    this.body = Array.of(this.from_login)

    this.jsonBody = JSON.parse(this.body);

    console.log('VALUE IN VIRTUAL DEPOSITS CONSTRUCTOR IS' + this.from_login);
    console.log('VALUE IN ARRACONSTRUCTOR IS' + this.jsonBody);

  }



  // displayNetwork(connectionState: string) {
  //   let networkType = this.network.type

  //   this.toastCtrl.create({
  //     message: "You are now " + "" + connectionState,
  //     duration: 5000
  //   }).present();



  // }


  // ionViewDidEnter() {

  //   this.network.onConnect().subscribe(data => {
  //     console.log(data)
  //     this.displayNetwork(data.type);
  //   }, error => console.error(error));
  //   this.network.onDisconnect().subscribe(data => {
  //     console.log(data)
  //     this.displayNetwork(data.type);
  //   }, error => console.error(error));

  // }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VirtualDepositPage');
  }

  deposit() {

    this.navCtrl.push(DepositPage, { 'value': this.body })
  }


  other() {

    this.navCtrl.push(OtherDepositPage, { 'value': this.body })
  }

}
  