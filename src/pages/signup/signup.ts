import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';
import { Http } from '@angular/http';
import { TabsPage } from '../tabs/tabs';
import { VerifyPage } from '../verify/verify';
import { ToastController, LoadingController, AlertController } from 'ionic-angular';
import { TransfersProvider } from '../../providers/transfers/transfers';
// import { DatabaseProvider } from '../../providers/database/database';
// import {SQLite, SQLiteObject} from '@ionic-native/sqlite';
import { Network } from '@ionic-native/network';
//import { FormBuilder, Validators } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Device } from '@ionic-native/device';
import 'rxjs/add/operator/map';

// const DATABASE_NAME: string = 'data.db';

interface deviceInterface {
  id?: string,

 
};

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
  animations: [

    //For the logo
    trigger('flyInBottomSlow', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({ transform: 'translate3d(0,2000px,0' }),
        animate('2000ms ease-in-out')
      ])
    ]),

    //For the background detail
    trigger('flyInBottomFast', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({ transform: 'translate3d(0,2000px,0)' }),
        animate('1000ms ease-in-out')
      ])
    ]),

    //For the login form
    trigger('bounceInBottom', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        animate('2000ms 200ms ease-in', keyframes([
          style({ transform: 'translate3d(0,2000px,0)', offset: 0 }),
          style({ transform: 'translate3d(0,-20px,0)', offset: 0.9 }),
          style({ transform: 'translate3d(0,0,0)', offset: 1 })
        ]))
      ])
    ]),

    //For login button
    trigger('fadeIn', [
      state('in', style({
        opacity: 1
      })),
      transition('void => *', [
        style({ opacity: 0 }),
        animate('1000ms 2000ms ease-in')
      ])
    ])
  ]
})
export class SignupPage {

  public signupForm: any;
  submitAttempt: boolean = false;

  messageList: any;

  api_code: any;

  signupVal: any;
  params: any;
  jsonBody: any;

  public itemList: Array<Object>;
public deviceInfo: deviceInterface = {};
  constructor(public navCtrl: NavController, public _form: FormBuilder, public toastCtrl: ToastController, public navParams: NavParams, public http: Http, public transfers: TransfersProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController, private device: Device) {
    this.deviceInfo.id = this.device.uuid;
    console.log("UUID IN SIGNUP " + this.deviceInfo.id )
    
    
    this.signupForm = this._form.group({
      "first_name": ["", Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      "last_name": ["", Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      "msisdn": ["", Validators.compose([Validators.maxLength(12), Validators.minLength(10), Validators.required])],
      "telco": ["", Validators.required],
      "alt_mobile_number": [""],

    })

  }

  // displayNetwork(connectionState: string) {
  //   let networkType = this.network.type

  //   this.toastCtrl.create({
  //     message: "You are now " + "" + connectionState,
  //     duration: 5000
  //   }).present();



  // }


  // ionViewDidEnter() {

  //   // this.network.onConnect().subscribe(data => {
  //   //   console.log(data)
  //   //   this.displayNetwork(data.type);
  //   // }, error => console.error(error));
  //   // this.network.onDisconnect().subscribe(data => {
  //   //   console.log(data)
  //   //   this.displayNetwork(data.type);
  //   // }, error => console.error(error));

  // }






  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }


  signup() {

    this.signupVal = JSON.stringify(this.signupForm.value);
    this.jsonBody = JSON.parse(this.signupVal);
    console.log("THIS IS THE SIGNUP VALUES" + this.jsonBody)


    this.signupVal = JSON.stringify(this.signupForm.value);
    this.jsonBody = JSON.parse(this.signupVal);
    console.log("THIS IS THE SIGNUP VALUES" + this.jsonBody)


    console.log("THIS IS THE SIGNUP VALUES stringified" + this.signupVal)
   
    console.log("LETS SEE first_name " + this.jsonBody.first_name)
      console.log("LETS SEE last_name " + this.jsonBody.last_name)
      console.log("LETS SEE msisdn " + this.jsonBody.msisdn)
      console.log("LETS SEE telco " + this.jsonBody.telco)
        console.log("LETS SEE alt_mobile_number " + this.jsonBody.alt_mobile_number)
          console.log("LETS SEE imei " + this.deviceInfo.id)
   

    this.params = {
      "first_name": this.jsonBody.first_name,
    "last_name": this.jsonBody.last_name, 
   "msisdn": this.jsonBody.msisdn,
    "telco": this.jsonBody.telco,
    "alt_mobile_number": this.jsonBody.alt_mobile_number,
    "imei": this.deviceInfo.id


    }


    let loader = this.loadingCtrl.create({
      content: "Please wait ..."
      
    });

    loader.present();
  
    this.transfers.signup(this.params).then((result) => {

      console.log(result);
      var jsonBody = result["_body"];
      console.log(jsonBody);

      jsonBody = JSON.parse(jsonBody);
      console.log(jsonBody)

      var desc = jsonBody["resp_desc"];
      var code = jsonBody["resp_code"];

      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;

      loader.dismiss();

      if (this.api_code == "801") {
        let loader = this.loadingCtrl.create({
          content: "Processing..."
         
        });
        loader.present();

        setTimeout(() => {
          this.navCtrl.setRoot(VerifyPage, { value: this.jsonBody });
        }, 3000);

        setTimeout(() => {
          loader.dismiss();
        }, 3000);

      }

      if (this.api_code != "000") {
        let alert = this.alertCtrl.create({
          title: "",
          subTitle: this.messageList,
          buttons: ['OK']
        });
        alert.present();
      }

    }, (err) => {
      loader.dismiss();
      this.toastCtrl.create({
        message: "Please check your internet connection",
        duration: 5000
      }).present();
      console.log(err);
    });
  }





















  //  private createDatabaseFile(): void {

  // this.sqlite.create({
  //   name: DATABASE_NAME,
  //   location: 'default'
  // })
  //   .then((db: SQLiteObject) => {

  //     console.log("CReating database");

  //     this.db = db;

  //     this.createTables();
  //     this.retrieveData(); 




  //   })
  //   .catch(e => console.log(e));

  // }



  // private createTables(): void {

  //    this.db.executeSql('CREATE TABLE IF NOT EXISTS REGISTRATION (id INTEGER PRIMARY KEY AUTOINCREMENT, first_name VARCHAR(255), last_name VARCHAR(255),alt_mobile_number VARCHAR(255),msisdn VARCHAR(255),id_type VARCHAR(255),id_num VARCHAR(255))', {})

  //       .then(() => console.log('registration table created!!'))
  //       .catch(e => console.log(e));


  // }



  // public signup(first_name){

  //     console.log("first name -->" + this.params.first_name)




  //  this.db.executeSql('INSERT INTO REGISTRATION (first_name) VALUES (\'' + this.params.first_name +'\')', {})

  //       .then(() => console.log('VALUES INSERTED OOOOOOOOOOOOOO!!'))
  //       .catch(e => console.log("AN ERROR OCCURED>>INSERT DIDNT HAPPEN" + e));


  // }


  // public retrieveData(){
  //   console.log('INSIDE RETRIVAL FUNCTION');

  //   this.registration = [];

  //    this.db.executeSql('SELECT first_name FROM registration limit 2 ', {})

  //       .then((data) => {

  //         if (data == null){
  //            console.log('DATA IS NULL');

  //           return;
  //         }



  //       if (data.rows) {

  //         console.log('RETRIEVAL ABOUT TO HAPPEN');

  //         if(data.rows.length > 0){

  //            console.log('DATA IS NOT NULL');
  //           for (var i = 0; i < data.rows.length; i++){
  //                 this.registration.push(


  //             data.rows.item(i)




  //                 );

  //                 console.log('RETRIEVAL COMPLETED');




  //             //return
  //           }

  //         }

  //       }


  //       });

  // }

  // }


  //  public onPageDidEnter() {
  //       this.load();
  //   }

  //   public load() {
  //       this.database.getPeople().then((result) => {
  //           this.itemList = <Array<Object>> result;
  //       }, (error) => {
  //           console.log("ERROR: ", error);
  //       });
  //   }

  // public create(first_name: string, last_name: string,alt_mobile_number:string,msisdn:string,id_type:string,id_num:string) {
  //     this.database.createPerson(first_name,last_name,alt_mobile_number,msisdn,id_type,id_num).then((result) => {
  //         this.load();
  //     }, (error) => {
  //         console.log("ERROR: ", error);
  //     });
  // }




}
