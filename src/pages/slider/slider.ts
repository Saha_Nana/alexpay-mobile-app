import { Component,ViewChild, ElementRef, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {Slides} from 'ionic-angular';
import { WelcomePage } from '../welcome/welcome';
import {waitRendered} from './util';


@Component({
  selector: 'page-slider',
  templateUrl: 'slider.html',
  //  templateUrl: 'build/pages/slider/slider.page.html'
})
export class SliderPage implements OnInit {

   splash = true;
    @ViewChild('mySlider')
private _slider:Slides;

  constructor(public navCtrl: NavController,private _elementRef:ElementRef, public navParams: NavParams) {
  }

   public ngOnInit() {
        let swiperContainer = this._elementRef.nativeElement.getElementsByClassName('swiper-container')[0];
        waitRendered(swiperContainer).then(()=>{
            let swiper = this._slider;
            swiper.update();
        });
}

  ionViewDidLoad() {
    setTimeout(() => this.splash = false, 4000);
    console.log('ionViewDidLoad SliderPage');
  }

   skip(){
    this.navCtrl.push(WelcomePage)
  }

}
