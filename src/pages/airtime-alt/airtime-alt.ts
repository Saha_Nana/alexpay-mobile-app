import { Component } from '@angular/core';
import { NavController, App, NavParams } from 'ionic-angular';
import { AirtimePage } from '../airtime/airtime';
import { AirtimeOtherPage } from '../airtime-other/airtime-other';
import { WelcomePage } from '../welcome/welcome';
import { LoadingController, AlertController } from 'ionic-angular';

/**
 * Generated class for the AirtimeAltPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-airtime-alt',
  templateUrl: 'airtime-alt.html',
})
export class AirtimeAltPage {

  messageList: any;

  api_code: any;

  from_login: any = [];

  body: any;

  data: any = [];

  jsonBody: any;
  params: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public _app: App) {

    this.from_login = this.navParams.get('value')

    this.body = Array.of(this.from_login)

    this.jsonBody = JSON.parse(this.body);

    console.log('VALUE IN AIRTIME CONSTRUCTOR IS' + this.from_login);
    console.log('VALUE IN ARRACONSTRUCTOR IS' + this.jsonBody);


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AirtimeAltPage');
  }


  self() {


    this.navCtrl.push(AirtimePage, { 'value': this.body });
  }

  other() {
    this.navCtrl.push(AirtimeOtherPage, { 'value': this.body });
  }



  showConfirm() {
    let confirm = this.alertCtrl.create({
      title: 'Logout?',
      message: 'Do you agree to logout ?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            let loader = this.loadingCtrl.create({
              content: "loggin out..."
              


            });

            loader.present();

            setTimeout(() => {
              this._app.getRootNav().setRoot(WelcomePage);
            }, 3000);

            setTimeout(() => {
              loader.dismiss();
            }, 3000);

          }
        }
      ]
    });
    confirm.present();
  }

}
