import { Component } from '@angular/core';
import { NavController,NavParams, App } from 'ionic-angular';

//import { TransferFundsPage } from '../transfer-funds/transfer-funds';
import { AirtimeAltPage } from '../airtime-alt/airtime-alt';
import { VirtualOptionPage } from '../virtual-option/virtual-option';
import { BillPage } from '../bill/bill';
import { HistoryPage } from '../history/history';
import { CustomerBalancePage } from '../customer-balance/customer-balance';

import { LoadingController,AlertController   } from 'ionic-angular';

import { WelcomePage } from '../welcome/welcome';
import { ProfilePage } from '../profile/profile';
import { Device } from '@ionic-native/device';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  from_login: any = [];
  body: any;

  constructor(public device: Device,public navCtrl: NavController,public loadingCtrl:LoadingController,public navParams: NavParams,public alertCtrl: AlertController, public _app: App) {
     this.from_login =  this.navParams.data

      this.body = Array.of(this.from_login)

         console.log('VALUE IN HOME CONSTRUCTOR IS' + this.from_login);
         console.log("THIS IS THE DEVICE UUID" + this.device.uuid)
         console.log("THIS IS THE DEVICE MODEL" + this.device.model)

  }

  transfer(){

    // this.navCtrl.push(TransferFundsPage,{value: this.from_login})  
    this.navCtrl.push(VirtualOptionPage,{value: this.from_login})

  }

  airtime(){

    this.navCtrl.push(AirtimeAltPage,{value: this.from_login})

  }

  logout(){

     let loader = this.loadingCtrl.create({
        content: "loggin out..."
        
       
      
      });

            loader.present();
     
       setTimeout(() => {
    this.navCtrl.setRoot(WelcomePage);
  }, 3000);

    setTimeout(() => {
    loader.dismiss();
  }, 3000);

    }




 


  

   bill(){

    this.navCtrl.push(BillPage,{value: this.from_login})


  }

   virtual(){

    this.navCtrl.push(VirtualOptionPage,{value: this.from_login})


  }


  balance(){

this.navCtrl.push(CustomerBalancePage,{value: this.from_login})

  }


  profile(){

    this.navCtrl.push(ProfilePage,{value: this.from_login});


  }


   showConfirm() {
    let confirm = this.alertCtrl.create({
      title: 'Logout?',
      message: 'Do you agree to logout ?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
             let loader = this.loadingCtrl.create({
        content: "Logging out...",
        duration: 5000
       
      
      });

            loader.present();
     
       setTimeout(() => {

    //  this._app.getRootNav().setRoot(LoginPagePage);
       // this.navCtrl.setRoot(WelcomePage);
         //this.navCtrl.setRoot(WelcomePage,{'value': this.from_login});
    this.navCtrl.parent.parent.setRoot(WelcomePage);
   //this._app.getRootNav().setRoot(WelcomePage,{'value': this.from_login});

  // this._app.getRootNavs()[0].setRoot(WelcomePage,{'value': this.from_login});
  // this.app.getRootNav().setRoot(SupportPage)
  }, 3000);

    setTimeout(() => {
    loader.dismiss();
  }, 3000);

          }
        }
      ]
    });
    confirm.present();
  }


summary(){


   let loader = this.loadingCtrl.create({
        content: "Generating History..."
     
       
      
      });

      loader.present();
   
       setTimeout(() => {
    this.navCtrl.push(HistoryPage,{value: this.from_login})
  }, 3000);

    setTimeout(() => {
    loader.dismiss();
  }, 3000);

  
 

}





  

}
