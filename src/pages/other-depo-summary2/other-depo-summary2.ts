import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';
import { Http } from '@angular/http';
import { OtherDepositPage } from '../other-deposit/other-deposit';
import { ToastController, LoadingController, AlertController } from 'ionic-angular';
import { TransfersProvider } from '../../providers/transfers/transfers';
import { Network } from '@ionic-native/network';
import { TabsPage } from '../tabs/tabs';

import 'rxjs/add/operator/map';

@Component({
  selector: 'page-other-depo-summary2',
  templateUrl: 'other-depo-summary2.html',
  animations: [

    //For the logo
    trigger('flyInBottomSlow', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({ transform: 'translate3d(0,2000px,0' }),
        animate('2000ms ease-in-out')
      ])
    ]),

    //For the background detail
    trigger('flyInBottomFast', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({ transform: 'translate3d(0,2000px,0)' }),
        animate('1000ms ease-in-out')
      ])
    ]),

    //For the login form
    trigger('bounceInBottom', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        animate('2000ms 200ms ease-in', keyframes([
          style({ transform: 'translate3d(0,2000px,0)', offset: 0 }),
          style({ transform: 'translate3d(0,-20px,0)', offset: 0.9 }),
          style({ transform: 'translate3d(0,0,0)', offset: 1 })
        ]))
      ])
    ]),

    //For login button
    trigger('fadeIn', [
      state('in', style({
        opacity: 1
      })),
      transition('void => *', [
        style({ opacity: 0 }),
        animate('1000ms 2000ms ease-in')
      ])
    ])
  ]
})
export class OtherDepoSummary2Page {

  messageList: any;

  api_code: any;

  // data: any;

  data: any = [];

  list: any = [];

  item: any = [];
  params: any = [];
  another: any = [];

  constructor(public navCtrl: NavController, private network: Network, public toastCtrl: ToastController, public navParams: NavParams, public http: Http, public transfers: TransfersProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public viewCtrl: ViewController) {
    this.list = this.navParams.get('value')

    this.another = this.navParams.get('another')

    console.log('VALUE IN CONSTRUCTOR IS' + this.list);
  }


  // displayNetwork(connectionState: string) {
  //   let networkType = this.network.type

  //   this.toastCtrl.create({
  //     message: "You are now " + "" + connectionState,
  //     duration: 5000
  //   }).present();



  // }


  // ionViewDidEnter() {

  //   this.network.onConnect().subscribe(data => {
  //     console.log(data)
  //     this.displayNetwork(data.type);
  //   }, error => console.error(error));
  //   this.network.onDisconnect().subscribe(data => {
  //     console.log(data)
  //     this.displayNetwork(data.type);
  //   }, error => console.error(error));

  // }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SummaryPage');

    this.list = this.navParams.get('value')
    console.log('VALUE IN VIEW IS' + this.list);
  }




  closeModal() {



    this.viewCtrl.dismiss();

  }



  confirm(list) {

    let loader = this.loadingCtrl.create({
      content: "Please wait ..."
      



    });

    loader.present();





    this.params = {
      "telco": list.telco,
      "mobile_number": list.msisdn,
      "amount": list.amount,
      "recipient_number": list.recipient_number,
      "recipient_telco": list.recipient_telco,
      "sender_name": list.sender_name,
      "voucher_code": list.voucher_code,
    }



    console.log("VALUE FROM HTML" + list);
    this.data = JSON.stringify(list)
    console.log(this.data)



    console.log("PARAMS" + this.params);



    this.transfers.transferFundsVirtual2(this.params).then((result) => {

      console.log("WE ARE IN THE FUNCTION");
      console.log("LETS SEE THE PARAMS" + this.data);
      console.log("LETS SEE THE LISTS" + this.list);


      console.log("THIS IS THE RESULT" + result);
      var jsonBody = result["_body"];
      console.log(jsonBody);

      jsonBody = JSON.parse(jsonBody);
      console.log(jsonBody)



      var desc = jsonBody["resp_desc"];
      var code = jsonBody["resp_code"];


      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;

      loader.dismiss();

      if (this.api_code == "000") {
        let loader = this.loadingCtrl.create({
          content: "Transaction processing...",
          

        });

        loader.present();
        setTimeout(() => {
          //this.navCtrl.push(OtherDepositPage,{'value': this.another});
          this.navCtrl.setRoot(TabsPage, { 'value': this.another });
          // this.navCtrl.pop();
          this.toastCtrl.create({
            message: "Check your phone for a prompt and enter your mobile money PIN to authorize the transaction..",
            duration: 5000
          }).present();
        }, 6000);

        setTimeout(() => {
          loader.dismiss();
        }, 6000);

      }

      if (this.api_code == "011") {
        let alert = this.alertCtrl.create({
          title: '',
          subTitle: this.messageList,
          buttons: ['OK']
        });

        alert.present();

      }



      if (this.api_code != "000") {
        let alert = this.alertCtrl.create({
          title: '',
          subTitle: this.messageList,
          buttons: ['OK']
        });

        alert.present();
        // this.navCtrl.push(OtherDepositPage,{'value': this.another});
        this.navCtrl.setRoot(TabsPage, { 'value': this.another });
        // this.navCtrl.pop();
        this.toastCtrl.create({
          message: "Transaction could not be processed. Please Try again..",
          duration: 5000
        }).present();
        alert.present();
      }


    }, (err) => {

      loader.dismiss();
      this.toastCtrl.create({
        message: "Please check your internet connection",
        duration: 5000
      }).present();

      console.log(err);
    });
  }
}
