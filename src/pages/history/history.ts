import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';
import { Http } from '@angular/http';
import { ToastController, LoadingController, AlertController } from 'ionic-angular';
import { TransfersProvider } from '../../providers/transfers/transfers';
import { Network } from '@ionic-native/network';
import 'rxjs/add/operator/map';




@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
  animations: [

    //For the logo
    trigger('flyInBottomSlow', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({ transform: 'translate3d(0,2000px,0' }),
        animate('2000ms ease-in-out')
      ])
    ]),

    //For the background detail
    trigger('flyInBottomFast', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({ transform: 'translate3d(0,2000px,0)' }),
        animate('1000ms ease-in-out')
      ])
    ]),

    //For the login form
    trigger('bounceInBottom', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        animate('2000ms 200ms ease-in', keyframes([
          style({ transform: 'translate3d(0,2000px,0)', offset: 0 }),
          style({ transform: 'translate3d(0,-20px,0)', offset: 0.9 }),
          style({ transform: 'translate3d(0,0,0)', offset: 1 })
        ]))
      ])
    ]),

    //For login button
    trigger('fadeIn', [
      state('in', style({
        opacity: 1
      })),
      transition('void => *', [
        style({ opacity: 0 }),
        animate('1000ms 2000ms ease-in')
      ])
    ])
  ]
})
export class HistoryPage {
  pet: string = "transfer";

  messageList: any;

  api_code: any;

  mobile_number: any;
  data: any = [];
  displayData: any;

  check: any;

  from_login: any = [];

  body: any;


  jsonBody: any;
  params: any = [];





  constructor(public navCtrl: NavController, private network: Network, public toastCtrl: ToastController, public navParams: NavParams, public http: Http, public transfers: TransfersProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {
    this.from_login = this.navParams.get('value')

    this.body = Array.of(this.from_login)

    this.jsonBody = JSON.parse(this.body);

    this.mobile_number = this.jsonBody[0].msisdn
    this.check = this.jsonBody[0]


    console.log('VALUE IN History CONSTRUCTOR IS' + this.from_login);
    console.log('VALUE IN ARRACONSTRUCTOR IS ' + this.mobile_number);


    this.params = {

      "mobile_number": this.mobile_number


    }

     console.log("------------DOC PARAMS----------" );
  console.log(this.params );

    this.transfers.history(this.params).then((result) => {

      console.log("RESULTS IS " + result);
      var body = result["_body"];
      body = JSON.parse(body);
      this.data = body
      console.log("RESULTS IS " + this.data);
      this.body = Array.of(this.data)


      var desc = body["resp_desc"];
      var code = body["resp_code"];


      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;


      if (this.api_code == "117") {
        let alert = this.alertCtrl.create({
          title: '',
          subTitle: this.messageList,
          buttons: ['OK']
        });
        // this.navCtrl.push(HomePage,{'value': this.from_login});
        this.navCtrl.pop();
        this.toastCtrl.create({
          message: "Transaction History not available..",
          duration: 5000
        }).present();
        alert.present();
      }


    }, (err) => {


      let alert = this.alertCtrl.create({
        title: '',
        subTitle: "Please check your internet connection",
        buttons: ['OK']

      });

      alert.present();


      this.navCtrl.pop();

      this.toastCtrl.create({
        message: "Please check your internet connection",
        duration: 5000
      }).present();


      console.log(err);
    });



  }

  displayNetwork(connectionState: string) {
    this.network.type
    // let networkType = this.network.type

    this.toastCtrl.create({
      message: "You are now " + "" + connectionState,
      duration: 5000
    }).present();



  }


  ionViewDidEnter() {

    this.network.onConnect().subscribe(data => {
      console.log(data)
      this.displayNetwork(data.type);
    }, error => console.error(error));
    this.network.onDisconnect().subscribe(data => {
      console.log(data)
      this.displayNetwork(data.type);
    }, error => console.error(error));

  }


  v_history() {

    this.params = {

      "mobile_number": this.mobile_number


    }

    this.transfers.v_history(this.params).then((result) => {

      var body = result["_body"];
      body = JSON.parse(body);
      this.data = body
      this.body = Array.of(this.data)


      var desc = body["resp_desc"];
      var code = body["resp_code"];


      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;


      if (this.api_code == "117") {
        let alert = this.alertCtrl.create({
          title: '',
          subTitle: this.messageList,
          buttons: ['OK']
        });
        // this.navCtrl.push(HomePage,{'value': this.from_login});
        this.navCtrl.pop();
        this.toastCtrl.create({
          message: "Transaction History not available..",
          duration: 5000
        }).present();
        alert.present();
      }


    }, (err) => {


      let alert = this.alertCtrl.create({
        title: '',
        subTitle: "Please check your internet connection",
        buttons: ['OK']
      });

      alert.present();

      this.navCtrl.pop();

      this.toastCtrl.create({
        message: "Please check your internet connection",
        duration: 5000
      }).present();


      console.log(err);
    });
  }





  t_history() {

    this.params = {

      "mobile_number": this.mobile_number


    }

    this.transfers.t_history(this.params).then((result) => {

      var body = result["_body"];
      body = JSON.parse(body);
      this.data = body
      this.body = Array.of(this.data)


      var desc = body["resp_desc"];
      var code = body["resp_code"];


      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;


      if (this.api_code == "117") {
        let alert = this.alertCtrl.create({
          title: '',
          subTitle: this.messageList,
          buttons: ['OK']
        });
        // this.navCtrl.push(HomePage,{'value': this.from_login});
        this.navCtrl.pop();
        this.toastCtrl.create({
          message: "Transaction History not available..",
          duration: 5000
        }).present();
        alert.present();
      }


    }, (err) => {


      let alert = this.alertCtrl.create({
        title: '',
        subTitle: "Please check your internet connection",
        buttons: ['OK']
      });

      alert.present();

      this.navCtrl.pop();

      this.toastCtrl.create({
        message: "Please check your internet connection",
        duration: 5000
      }).present();


      console.log(err);
    });
  }






  b_history() {

    this.params = {

      "mobile_number": this.mobile_number


    }

    this.transfers.b_history(this.params).then((result) => {

      var body = result["_body"];
      body = JSON.parse(body);
      this.data = body
      this.body = Array.of(this.data)


      var desc = body["resp_desc"];
      var code = body["resp_code"];


      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;


      if (this.api_code == "117") {
        let alert = this.alertCtrl.create({
          title: '',
          subTitle: this.messageList,
          buttons: ['OK']
        });
        // this.navCtrl.push(HomePage,{'value': this.from_login});
        this.navCtrl.pop();
        this.toastCtrl.create({
          message: "Transaction History not available..",
          duration: 5000
        }).present();
        alert.present();
      }


    }, (err) => {


      let alert = this.alertCtrl.create({
        title: '',
        subTitle: "Please check your internet connection",
        buttons: ['OK']
      });

      alert.present();

      this.navCtrl.pop();

      this.toastCtrl.create({
        message: "Please check your internet connection",
        duration: 5000
      }).present();


      console.log(err);
    });
  }







  f_history() {

    this.params = {

      "mobile_number": this.mobile_number


    }

    this.transfers.history(this.params).then((result) => {

      var body = result["_body"];
      body = JSON.parse(body);
      this.data = body
      this.body = Array.of(this.data)


      var desc = body["resp_desc"];
      var code = body["resp_code"];


      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;


      if (this.api_code == "117") {
        let alert = this.alertCtrl.create({
          title: '',
          subTitle: this.messageList,
          buttons: ['OK']
        });
        // this.navCtrl.push(HomePage,{'value': this.from_login});
        this.navCtrl.pop();
        this.toastCtrl.create({
          message: "Transaction History not available..",
          duration: 5000
        }).present();
        alert.present();
      }


    }, (err) => {


      let alert = this.alertCtrl.create({
        title: '',
        subTitle: "Please check your internet connection",
        buttons: ['OK']
      });

      alert.present();

      this.navCtrl.pop();

      this.toastCtrl.create({
        message: "Please check your internet connection",
        duration: 5000
      }).present();


      console.log(err);
    });
  }















}

