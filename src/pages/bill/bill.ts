import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';
import { Http } from '@angular/http';
import { ToastController, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { TransfersProvider } from '../../providers/transfers/transfers';
import { BillSummaryPage } from '../bill-summary/bill-summary';
import { CallNumber } from '@ionic-native/call-number';
import { Network } from '@ionic-native/network';
import 'rxjs/add/operator/map';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';



@Component({
  selector: 'page-bill',
  templateUrl: 'bill.html',
  animations: [

    //For the logo
    trigger('flyInBottomSlow', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({ transform: 'translate3d(0,2000px,0' }),
        animate('2000ms ease-in-out')
      ])
    ]),

    //For the background detail
    trigger('flyInBottomFast', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({ transform: 'translate3d(0,2000px,0)' }),
        animate('1000ms ease-in-out')
      ])
    ]),

    //For the login form
    trigger('bounceInBottom', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        animate('2000ms 200ms ease-in', keyframes([
          style({ transform: 'translate3d(0,2000px,0)', offset: 0 }),
          style({ transform: 'translate3d(0,-20px,0)', offset: 0.9 }),
          style({ transform: 'translate3d(0,0,0)', offset: 1 })
        ]))
      ])
    ]),

    //For login button
    trigger('fadeIn', [
      state('in', style({
        opacity: 1
      })),
      transition('void => *', [
        style({ opacity: 0 }),
        animate('1000ms 2000ms ease-in')
      ])
    ])
  ]
})
export class BillPage {

  public billForm: any;
  submitAttempt: boolean = false;
  jsonBody1: any;
  billVal: any;

  messageList: any;

  api_code: any;

  from_login: any = [];

  body: any;

  data: any = [];

  jsonBody: any;
  params: any = [];

   first_name: any;
  last_name: any;
  mobile: any;
  telco: any;

  constructor(public navCtrl: NavController, public _form: FormBuilder, private network: Network, public toastCtrl: ToastController, public navParams: NavParams, public http: Http, public transfers: TransfersProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public modalCtrl: ModalController, private callNumber: CallNumber) {
    this.from_login = this.navParams.get('value')

    this.body = Array.of(this.from_login)

    this.jsonBody = JSON.parse(this.body);

    console.log('VALUE IN BILL CONSTRUCTOR IS' + this.from_login);
    console.log('VALUE IN ARRACONSTRUCTOR IS' + this.jsonBody);

     this.first_name = this.jsonBody[0].first_name
    this.last_name = this.jsonBody[0].last_name
    this.mobile = this.jsonBody[0].msisdn
    this.telco = this.jsonBody[0].telco
    
    console.log("Name " +  this.first_name + this.last_name + this.mobile +  this.telco  )


    this.billForm = this._form.group({
      "voucher_code": ["", Validators.compose([Validators.maxLength(6), Validators.minLength(6)])],
      "amount": ["", Validators.compose([Validators.minLength(1), Validators.required])],
      "smart_card_num": ["", Validators.compose([Validators.maxLength(12), Validators.minLength(10), Validators.required])],
      "bill_type": ["", Validators.required]
    })


  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad BillPage');
  }


  call(n: string) {
    this.callNumber.callNumber(n, true)
      .then(() => console.log('Launched dialer!'))
      .catch(() => console.log('Error launching dialer'));
  }


  summary() {

    this.billVal = JSON.stringify(this.billForm.value);
    this.jsonBody1 = JSON.parse(this.billVal);
    console.log("THIS IS THE SIGNUP VALUES" + this.jsonBody)
    console.log("THIS IS THE billForm VALUES" + this.jsonBody1)
    console.log("THIS IS THE billForm VALUES stringified" + this.billVal)
    console.log("LETS SEE sender_name " + this.jsonBody[0].first_name + " " + this.jsonBody[0].last_name)
    console.log("LETS SEE MOBILE NUMBER " + this.jsonBody[0].msisdn)
     console.log("LETS SEE telco " + this.jsonBody[0].telco)
   
   this.jsonBody1 = {
      "sender_name": this.jsonBody[0].first_name + " " + this.jsonBody[0].last_name,
      "msisdn": this.jsonBody[0].msisdn,
      "telco": this.jsonBody[0].telco,
       "amount": this.jsonBody1.amount,
      "smart_card_num": this.jsonBody1.smart_card_num,
      "bill_type": this.jsonBody1.bill_type,
      "voucher_code": this.jsonBody1.voucher_code
    }

    this.data = JSON.stringify(this.jsonBody1)
    console.log("STRINGIFIED DATA IS" + this.data);


    this.body = Array.of(this.jsonBody1)

    console.log("ARRAY DATA IS" + this.body);
    console.log("PARAM DATA IS" + this.jsonBody1);



    let modal = this.modalCtrl.create(BillSummaryPage, {

      'value': this.body,
      'another': this.from_login
    });

    modal.present();


  }




}
