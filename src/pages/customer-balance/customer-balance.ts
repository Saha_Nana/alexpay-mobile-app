import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import { ToastController, LoadingController, AlertController } from 'ionic-angular';
import { TransfersProvider } from '../../providers/transfers/transfers';
import { Network } from '@ionic-native/network';
import 'rxjs/add/operator/map';
import { TabsPage } from '../tabs/tabs';


// @IonicPage()
@Component({
  selector: 'page-customer-balance',
  templateUrl: 'customer-balance.html',
})
export class CustomerBalancePage {


  messageList: any;

  api_code: any;
  list: any = [];
  mobile_number: any;
  data: any;
  displayData: any;

  check: any;

  from_login: any = [];

  body: any;


  jsonBody: any;
  params: any = [];
  another: any = [];

  constructor(public navCtrl: NavController, private network: Network, public toastCtrl: ToastController, public navParams: NavParams, public http: Http, public transfers: TransfersProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {
    this.from_login = this.navParams.get('value')

    this.body = Array.of(this.from_login)

    this.jsonBody = JSON.parse(this.body);

    this.another = this.navParams.get('another')

    console.log('VALUE IN BILL CONSTRUCTOR IS' + this.from_login);
    console.log('VALUE IN ARRACONSTRUCTOR IS' + this.jsonBody);




  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad CustomerBalancePage');
  }



  balance() {
    
    let loader = this.loadingCtrl.create({
      content: "Please wait ..."

    });

    loader.present();

    this.params = {
      "mobile_number": this.jsonBody[0].msisdn,
      "pin": this.params.pin

    }




    this.transfers.balanceCheck(this.params).then((result) => {

      console.log(result);
      var jsonBody = result["_body"];
      console.log(jsonBody);

      jsonBody = JSON.parse(jsonBody);
      console.log(jsonBody)

      var desc = jsonBody["resp_desc"];
      var code = jsonBody["resp_code"];


      console.log(desc);
      console.log(code);

      this.messageList = desc;
      this.api_code = code;

      loader.dismiss();

      if (this.api_code == "000") {
        let alert = this.alertCtrl.create({
          title: 'Hello!',
          subTitle: this.messageList,
          buttons: ['OK']
        });
        alert.present();
      }

      this.navCtrl.setRoot(TabsPage, { 'value': this.from_login });

      if (this.api_code != "000") {
        let alert = this.alertCtrl.create({
          title: '',
          subTitle: this.messageList,
          buttons: ['OK']
        });
        alert.present();
      }

      this.navCtrl.setRoot(TabsPage, { 'value': this.from_login });



    }, (err) => {
      loader.dismiss();
      this.toastCtrl.create({
        message: "Please check your internet connection",
        duration: 5000
      }).present();

      console.log(err);
    });
  }



}
