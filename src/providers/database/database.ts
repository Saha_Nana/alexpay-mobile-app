import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
// import {SQLite} from 'ionic-native';
import {SQLite, SQLiteObject} from '@ionic-native/sqlite';
import 'rxjs/add/operator/map';

/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class DatabaseProvider {

//   private storage: SQLite;
    private isOpen: boolean;

   private db:SQLiteObject;

  constructor(public http: Http, private sqlite: SQLite) { 


if(!this.isOpen) {
this.sqlite.create({
  name: 'data.db',
  location: 'default'
})
  .then((db: SQLiteObject) => {


    db.executeSql('create table IF NOT EXISTS registration(id INTEGER PRIMARY KEY AUTOINCREMENT, first_name VARCHAR(255), last_name VARCHAR(255),alt_mobile_number VARCHAR(255),msisdn VARCHAR(255),id_type VARCHAR(255),id_num VARCHAR(255))', {})

      .then(() => console.log('Executed SQL'))
      .catch(e => console.log(e));

 this.isOpen = true;

  })
  .catch(e => console.log(e));

  }

}


 public getPeople() {
        return new Promise((resolve, reject) => {
            this.db.executeSql("SELECT * FROM registration", []).then((data) => {
                let registration = [];
                if(data.rows.length > 0) {
                    for(let i = 0; i < data.rows.length; i++) {
                        registration.push({
                            id: data.rows.item(i).id,
                            first_name: data.rows.item(i).firstname,
                            last_name: data.rows.item(i).lastname,
                            alt_mobile_number: data.rows.item(i).alt_mobile_number,
                            msisdn: data.rows.item(i).msisdn,
                            id_type: data.rows.item(i).id_type,
                            id_num: data.rows.item(i).id_num
                        });
                    }
                }
                resolve(registration);
            }, (error) => {
                reject(error);
            });
        });
    }
 
    public createPerson(first_name: string, last_name: string,alt_mobile_number:string,msisdn:string,id_type:string,id_num:string ) {
        console.log("-----------------------------------------");
   console.log("USING CREATE PERSON FUNCTION");
   console.log("-----------------------------------------");
      
        return new Promise((resolve, reject) => {
            this.db.executeSql("INSERT INTO people (first_name, last_name,alt_mobile_number,msisdn,id_type,id_num) VALUES (?, ?,?,?,?,?)", [first_name, last_name,alt_mobile_number,msisdn,id_type,id_num]).then((data) => {
                resolve(data);
            }, (error) => {
                reject(error);
            });
        });
    }
 
}
