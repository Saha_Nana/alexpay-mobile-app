import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class TransfersProvider {

  constructor(public http: Http) {
    console.log('Hello TransfersProvider Provider');
  }
  
  apiUrl = 'https://appsnmobileagent.com:6075/ap_transfer_networks';
   trans = 'https://appsnmobileagent.com:6075/send_others_alexpay';
   trans2 = 'https://appsnmobileagent.com:6075/send_others_alexpay2';
   self_airtimeUrl = 'https://appsnmobileagent.com:6075/self_top_up';
   others_airtimeUrl = 'https://appsnmobileagent.com:6075/other_top_up';
   bill_Url = 'https://appsnmobileagent.com:6075/bill_payment';
   login_Url = 'https://appsnmobileagent.com:6075/login_v2';
    
    //signup_Url = 'http://67.205.74.208:6088/ap_register_v2';
   // verify_Url = 'http://67.205.74.208:6088/user_verify';
   // login_Url = 'http://67.205.74.208:6088/login';
    
     signup_Url = 'https://appsnmobileagent.com:6075/ap_register_v2';
    verify_Url = 'https://appsnmobileagent.com:6075/user_verify';
   with_Url ='https://appsnmobileagent.com:6075/self_virtual_withdrawal';
    dep_Url ='https://appsnmobileagent.com:6075/self_virtual_deposits';
    otherdep_Url ='https://appsnmobileagent.com:6075/thirdparty_virtual_deposits';
    bal_Url ='https://appsnmobileagent.com:6075/check_balance';
     his_Url ='https://appsnmobileagent.com:6075/transfer_history';
      virtual_history_Url ='https://appsnmobileagent.com:6075/virtual_history';
       top_history_Url ='https://appsnmobileagent.com:6075/top_history';
       bill_history_Url ='https://appsnmobileagent.com:6075/bill_history';
     pro_Url ='https://appsnmobileagent.com:6075/change_pin';
      retrieve_Url ='https://appsnmobileagent.com:6075/retrieve_sub';


transferFunds(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.apiUrl, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}


transferFundsVirtual(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.trans, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}


transferFundsVirtual2(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.trans2, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err); 
      });
  });
}


airtimeSelf(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.self_airtimeUrl, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}


airtimeOther(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.others_airtimeUrl, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}


billpayment(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.bill_Url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}


login(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.login_Url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}


signup(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.signup_Url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

apiVerify(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.verify_Url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

withdrawal(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.with_Url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

deposit(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.dep_Url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

otherdeposit(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.otherdep_Url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}


balanceCheck(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.bal_Url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

history(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.his_Url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

v_history(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this. virtual_history_Url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

t_history(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this. top_history_Url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}


b_history(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this. bill_history_Url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}

profile(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.pro_Url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}




retrieve(data) {
  return new Promise((resolve, reject) => {
    this.http.post(this.retrieve_Url, JSON.stringify(data))
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}



}
