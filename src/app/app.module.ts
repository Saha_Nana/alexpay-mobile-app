import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { TransferFundsPage } from '../pages/transfer-funds/transfer-funds';
import { VirtualOptionPage } from '../pages/virtual-option/virtual-option';
import { VirtualDepositPage } from '../pages/virtual-deposit/virtual-deposit';
import { VirtualWithdrawalPage } from '../pages/virtual-withdrawal/virtual-withdrawal';
import { WelcomePage } from '../pages/welcome/welcome';
import { AirtimeAltPage } from '../pages/airtime-alt/airtime-alt';
import { CustomerBalancePage } from '../pages/customer-balance/customer-balance';
import { AirtimeOtherPage } from '../pages/airtime-other/airtime-other';
import { SignupPage } from '../pages/signup/signup';
import { SelfTopSummaryPage } from '../pages/self-top-summary/self-top-summary';
import { OtherTopSummaryPage } from '../pages/other-top-summary/other-top-summary';
import { SelfDepoSummaryPage } from '../pages/self-depo-summary/self-depo-summary';
import { SelfWithSummaryPage } from '../pages/self-with-summary/self-with-summary';
import { OtherDepoSummaryPage } from '../pages/other-depo-summary/other-depo-summary';
import { OtherDepoSummary2Page } from '../pages/other-depo-summary2/other-depo-summary2';
import { LoginPage } from '../pages/login/login';
import { AirtimePage } from '../pages/airtime/airtime';
import { BillPage } from '../pages/bill/bill';
import { DepositPage } from '../pages/deposit/deposit';
import { VerifyPage } from '../pages/verify/verify';
import { HistoryPage } from '../pages/history/history';
import { ProfilePage } from '../pages/profile/profile';
import { BillSummaryPage } from '../pages/bill-summary/bill-summary';
import { OtherDepositPage } from '../pages/other-deposit/other-deposit';
import { TransactionHistoryPage } from '../pages/transaction-history/transaction-history';
import { SliderPage } from '../pages/slider/slider';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TransfersProvider } from '../providers/transfers/transfers';

import { HttpModule } from '@angular/http';
import {Network} from '@ionic-native/network';
import { Device } from '@ionic-native/device';
import { CallNumber } from '@ionic-native/call-number';
import {SQLite} from '@ionic-native/sqlite';
import { DummyPage } from '../pages/dummy/dummy';
import { SummaryPage } from '../pages/summary/summary';
import { Summary2Page } from '../pages/summary2/summary2';
import { DatabaseProvider } from '../providers/database/database';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    WelcomePage,
    SignupPage,
    TransferFundsPage,
    AirtimePage,
    AirtimeAltPage,
    AirtimeOtherPage,
    BillPage,
    DummyPage,
    LoginPage,
    VirtualOptionPage,
    VirtualWithdrawalPage,
    VirtualDepositPage,
    DepositPage,
    OtherDepositPage,
    CustomerBalancePage,
    HistoryPage,
    TransactionHistoryPage,
    ProfilePage,
    SliderPage,
    SummaryPage,
    BillSummaryPage,
    SelfTopSummaryPage,
    OtherTopSummaryPage,
    SelfDepoSummaryPage,
    OtherDepoSummaryPage,
    SelfWithSummaryPage,
    Summary2Page,
    OtherDepoSummary2Page,
    VerifyPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    WelcomePage,
    SignupPage,
    TransferFundsPage,
    AirtimePage,
    AirtimeAltPage,
    AirtimeOtherPage,
    BillPage,
    LoginPage,
    VirtualOptionPage,
    VirtualWithdrawalPage,
    VirtualDepositPage,
    DepositPage,
    OtherDepositPage,
    CustomerBalancePage,
    HistoryPage,
    TransactionHistoryPage,
    ProfilePage,
    SliderPage,
    SummaryPage,
    BillSummaryPage,
    SelfTopSummaryPage,
    OtherTopSummaryPage,
    SelfDepoSummaryPage,
    OtherDepoSummaryPage,
    SelfWithSummaryPage,
    Summary2Page,
    OtherDepoSummary2Page,
    VerifyPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    TransfersProvider,
    Network,
    Device,
    DatabaseProvider,
    SQLite,
    CallNumber
  ]
})
export class AppModule {}
